# Java hors les livres...

L'idée de ce TP est de simuler une application de gestion de bibliothèque.
Notre bibliothèque contient les informations suivantes :
* Un nom de propriétaire
* Une collection de livres

Dans notre bibliothèque, nous gèrerons un ensemble de livres caractérisés par :
* un numéro ISBN
* un titre
* un ou plusieurs auteurs
* un éditeur
* un nombre de pages


## Mise en place
Afin de mettre en place cette application, je vous propose différentes
étapes de réalisation.

Afin de valider leur bon fonctionnement, vous pouvez utilisez la classe Demo
pour y manipuler vos données...

**Rappel ouverture du projet dans Intellij :**
Dans Intellij, ouvrez le fichier `pom.xml` du projet, et choississez de
l'ouvrir *'en tant que projet'*.


### Première étape

Pour cette première étape, nous commencerons par avoir simplement une bibliothèque. \
Pour cela, on attend la fonctionnalité suivante :
* Créer une bibliothèque (avec le nom du propriétaire)

### Seconde étape

Maintenant que nous avons notre bibliothèque, elle semble un peu vide :
il est temps d'ouvrir les cartons et de ranger nos livres ! \
Il est donc nécessaire d'ajouter les fonctionnalités suivantes à notre bibliothèque :
* ajouter un livre


### Troisème étape

Une bibliothèque, avec des livres dedans, on devrait pouvoir savoir si
on a déjà un livre, et pourquoi pas s'en séparer (snif!). \
Ajoutons donc les fonctionnalités suivantes :
* savoir si la bibliothèque contient un livre avec son ISBN
* supprimer un livre de la bibliothèque (depuis son ISBN ?)


### Quatrième étape

Ce serait intéressant d'apporter un peu d'interractivité à notre application.
Pour cela, on aimerait qu'au lancement de l'application, celle-ci nous
demande ce que l'on souhaite faire :
* ajouter un livre
* savoir si la bibliothèque contient déjà un livre
* supprimer un livre

En fonction de notre choix, elle va nous proposer d'entrer les éléments
nécessaires :
* Le numéro ISBN pour la recherche ou la suppression
* Les différentes informations nécessaires à la création d'un livre


Bon, je pense qu'on commence à avoir une solution correcte pour notre bibliothèque là.

## Allez plus loin ?

On a notre bliothèque, on ajoute nos livres, on sait si on les a déjà et
on les supprime. La base est là, mais... ce serait cool si on pouvait
avoir plus d'information sur nos livres, en fonction de ce qu'ils sont.
Après tout, un auteur c'est restrictif, on pourrait avoir des illustrateurs
par exemple. Mais ça ne correspond pas à tous les types de livre...

### Cinquième étape (bonus) : types de livre

Dans cette étape, on va considérer plusieurs types de livres, chacun
avec ses caractéristiques propres en plus de celles du livre :
* Roman
* Bande dessinée (illustrateur(s), tome)
* Encyclopédie (thème)
* ArtBook (sujet)
* ...

Vous aurez compris que l'idée est d'éviter au possible la redondance de code.

**Attention :** il conviendra de prendre ces nouveautés en compte lors de la
création d'un livre notamment.

### Sixième étape (bonus) : enregistrer les données

Ce serait quand même plus pratique si au lancement de l'application, nous
pouvions récupérer les données déjà saisies dans l'application.


### Septième étape (bonus) : rechercher des livres

Notre application nous permet actuellement de retrouver un livre depuis
la bibliothèque grâce à son numéro ISBN. C'est cependant pas suffisant,
et on aimerait pouvoir rechercher nos livres en fonction du titre.

Dans l'idée, l'utilisateur saisit le titre complet ou une partie, et
l'application lui retourne la liste des livres correspondant.
Un petit indice ? Il est possible de vérifier si une chaine de caractères
contient une autre chaine de caractères.

(Et si vous êtes joueurs, vous pouvez essayer de faire des streams & lambdas ;)

### Huitième étape (bonus de bonus) : rechercher par auteur

Tout est dans le titre ! On saisit le nom d'un auteur, et on récupère
l'ensemble de ses livres présents dans la bibliothèque